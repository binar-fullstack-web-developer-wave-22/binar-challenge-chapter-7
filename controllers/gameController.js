//gameController
const { MatchHistoryExtend, User, Room, MatchHistory } = require('../models');
// const room = require('../models/room');

function theRoundResult(playerData1, playerData2) {
  const { playerData1, playerData2 } = req.body.choice;
  let theResult = '';
  if (playerData1 == playerData2) return 'DRAW';
  //   if (playerData1 == 'R' return playerData2 == 'P' ? (theResult = 'LOSE') : 'WIN';
  //   if (playerData1 == 'P') return playerData2 == 'R' ? (theResult = 'WIN') : 'LOSE';
  //   if (playerData1 == 'S') return playerData2 == 'P' ? (theResult = 'WIN') : 'LOSE';

  if (playerData1 == 'R' && playerData2 == 'P') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'LOSE' },
      { player: playerData2.user_id, Result: 'WIN' },
    ]);
  }
  if (playerData1 == 'R' && playerData2 == 'S') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'WIN' },
      { player: playerData2.user_id, Result: 'LOSE' },
    ]);
  }
  if (playerData1 == 'P' && playerData2 == 'R') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'WIN' },
      { player: playerData2.user_id, Result: 'LOSE' },
    ]);
  }
  if (playerData1 == 'P' && playerData2 == 'S') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'LOSE' },
      { player: playerData2.user_id, Result: 'WIN' },
    ]);
  }
  if (playerData1 == 'S' && playerData2 == 'P') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'WIN' },
      { player: playerData2.user_id, Result: 'LOSE' },
    ]);
  }
  if (playerData1 == 'S' && playerData2 == 'R') {
    return (theResult = [
      { player: playerData1.user_id, Result: 'LOSE' },
      { player: playerData2.user_id, Result: 'WIN' },
    ]);
  }
  return theResult;
}

// const p1Data = p1score : 0;
// const p2Data = p2score : 0:

// function getScore(matchHistoryPerRoom, score1, score2) {
//   MatchHistory.create({
//     room_id: matchHistoryPerRoom[0].room_id,
//     user_id:matchHistoryPerRoom[0].user_id,
//     finalScore: score1,
//     score: p1Data
//   })
//   MatchHistory.create({
//     room_id: matchHistoryPerRoom[1].room_id,
//     user_id:matchHistoryPerRoom[1].user_id,
//     finalScore: score2,
//     score: p2Data
//   })
//   p1Data = 0;
//   p2Data = 0;
// } on works

function theOverallResult(matchHistoryPerRoom) {
  if (matchHistoryPerRoom.length === 6) {
    if (p1Data > p2Data) {
      return 'SubjectOne is the Winner';
    } else if (p1Data > p2Data) {
      return 'SubjectTwo is the Winner';
    } else {
      return 'The match is draw';
    }
  }
}

function theMatchResult(matchHistoryPerRoom) {
  const dataRounds = [];
  let round = 1;
  for (let i = 0; i < matchHistoryPerRoom.length; i += 2) {
    dataRounds.push({
      round,
      result: theRoundResult(matchHistoryPerRoom[i], matchHistoryPerRoom[i + 1]),
      choices: [
        {
          user_id: matchHistoryPerRoom[i].user_id,
          choice: matchHistoryPerRoom[i].choice,
        },
        {
          user_id: matchHistoryPerRoom[i + 1].user_id,
          choice: matchHistoryPerRoom[i + 1].choice,
        },
      ],
    });
    round++;
  }
  return {
    finalResult: theOverallResult(matchHistoryPerRoom),
    rounds: dataRounds,
  };
}

module.exports = {
  list: async (req, res, next) => {
    const playerData = await MatchHistoryExtend.findAll({
      where: { room_id: +req.params.listId },
    });
    res.json(playerData);
  },
  fight: async (req, res, next) => {
    //Memanggil static method register yg sudah dibuat
    const { roomId } = req.params;
    const { choice } = req.body;
    const { id } = req.user;

    MatchHistoryExtend.create({ room_id: roomId, user_id: id, choice });
    let matchHistoryPerRoom = await MatchHistoryExtend.findAll({ room_id: roomId });
    if (matchHistoryPerRoom.length % 2 === 1) {
      const theInterval = setInterval(async () => {
        matchHistoryPerRoom = await MatchHistoryExtend.findAll({
          where: { room_id: roomId },
          include: User,
        });

        if (matchHistoryPerRoom.length % 2 === 0) {
          clearInterval(theInterval);

          res.json(theMatchResult(matchHistoryPerRoom));
        }
      }, 5000);
    } else {
      res.json(theMatchResult(matchHistoryPerRoom));
    }

    // res.json({ userId: id, roomId, choice });
  },
};
