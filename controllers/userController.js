//authController
const { User } = require('../models');
// const paspport = require('../lib/passport');

function format(user, includeToken) {
  const { id, username } = user;
  const formattedUser = {
    id,
    username,
  };

  if (includeToken) {
    formattedUser.accessToken = user.generateToken();
  }
  return formattedUser;
}

module.exports = {
  register: (req, res, next) => {
    //Memanggil static method register yg sudah dibuat
    User.register(req.body)
      .then(() => {
        res.json('Register success');
      })
      .catch((err) => next(err));
  },
  login: (req, res) => {
    User.authenticate(req.body).then((user) => {
      res.json(format(user, true));
    });
  },
  whoami: (req, res) => {
    res.json(format(req.user, false));
  },
};
