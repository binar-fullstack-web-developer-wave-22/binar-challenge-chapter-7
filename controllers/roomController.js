const { Room } = require('../models');

module.exports = {
  list: (req, res) => {
    const allRoom = Room.findAll()
      .then(() => {
        res.json(allRoom);
      })
      .catch((err) => next(err));
  },
  create: (req, res) => {
    const { name } = req.body;
    const room = Room.create({ name })
      .then(() => {
        res.json('New room created');
      })
      .catch((err) => next(err));
  },
};
