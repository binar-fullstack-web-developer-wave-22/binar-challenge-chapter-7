var express = require('express');
var router = express.Router();
const games = require('../controllers/gameController');
const roomValidation = require('../middlewares/roomValidation');

router.post('/fight/:roomId', roomValidation, games.fight);

module.exports = router;
