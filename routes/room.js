var express = require('express');
var router = express.Router();
const room = require('../controllers/roomController');
const isAuthenticated = require('../middlewares/isAuthenticated');

router.get('/room', isAuthenticated, room.list);
router.post('/create', isAuthenticated, room.create);

module.exports = router;
