const { MatchHistoryExtend } = require('../models');
const matchhistoryextend = require('../models/matchhistoryextend');
const room = require('../models/room');

module.exports = async (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.json({ message: 'Please Login first' });
  }

  const { roomId } = req.params;
  const { id } = req.user;
  const matchHistoryPerRoom = await MatchHistoryExtend.findAll({ room_id: roomId });
  if (matchHistoryPerRoom.length === 6) {
    //jika data sudah 6
    return res.json({ message: 'Cannot use the room' });
  }

  if (matchHistoryPerRoom.length < 2) {
    //Jika sudah 2 player
    return next();
  }

  if (!(matchHistoryPerRoom[0].user_id === id || matchHistoryPerRoom[1].user_id === id)) {
    return res.json({ message: 'Please choose another room' });
  }

  next();
};
