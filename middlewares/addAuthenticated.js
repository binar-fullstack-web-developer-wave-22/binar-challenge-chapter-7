const passport = require('../lib/passport');

module.exports = (req, res, next) => {
  //Jika request berasal dari user yg terautentifikasi,
  //maka akan lanjut menjalankan handler selanjutnya
  passport.authenticate(
    'jwt',
    {
      session: false,
    },
    (err, user, info) => {
      if (err || !user) {
        req.isAuthenticated = () => false;
      } else {
        req.isAuthenticated = () => true;
        req.user = user;
      }
      return next();
    }
  )(req, res, next);
};
