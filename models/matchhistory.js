'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MatchHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Room, { foreignKey: 'room_id' });
      this.belongsTo(models.User, { foreignKey: 'user_id' });
    }
  }
  MatchHistory.init(
    {
      room_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: { model: 'Rooms', key: 'id' },
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: { model: 'Users', key: 'id' },
      },
      result: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      score: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
    },
    {
      sequelize,
      modelName: 'MatchHistory',
    }
  );
  return MatchHistory;
};
