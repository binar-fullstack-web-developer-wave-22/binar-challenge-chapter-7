'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.MatchHistory, {
        foreignKey: 'user_id',
      });
      this.hasMany(models.MatchHistoryExtend, {
        foreignKey: 'user_id',
      });
    }

    generateToken = () => {
      //Jangan memasukan password ke dalam payload
      const payload = {
        id: this.id,
        username: this.username,
      };
      //Rahasia ini nantinya dipakai untuk verifikasi apakah token ini berasal dari app kita
      const secretPass = 'Top secret stuff';
      //Membuat token dari data diatas
      const token = jwt.sign(payload, secretPass);
      return token;
    };

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password);
      /*
        #encrypt dari static method
        encryptedPassword akan sama dengan string 
        hasil enkripsi password dari method #encrypt
      */
      return this.create({ username, password: encryptedPassword });
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject('User not found!');
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject('Password is Invalid!');
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }

  User.init(
    {
      username: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING,
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: 'User',
    }
  );
  return User;
};
